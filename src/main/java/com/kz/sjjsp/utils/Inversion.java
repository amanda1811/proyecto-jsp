package com.kz.sjjsp.utils;

import com.kz.sjjsp.impl.ClienteAc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * @author Kz
 */
@Configuration
@EnableWebMvc
@ComponentScan("com.kz")
public class Inversion {

    @Bean
    InternalResourceViewResolver viewRes() {
        InternalResourceViewResolver r = new InternalResourceViewResolver();
        r.setPrefix("/");
        r.setSuffix(".jsp");
        return r;
    }
    
    @Bean
    public Connection getConex(){
        Connection c;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            //Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/facturacion?useSSL=false", "root", "rootroot");
            //c = DriverManager.getConnection("jdbc:mysql://localhost:3306/facturacion?useSSL=false", "root", "rootroot");
        } catch (SQLException e) {
            c = null;
        }
        catch(ClassNotFoundException cx){
            c = null;
        }
        return c;
    }

    @Bean
    public Dao clients() {
        return new ClienteAc();
    }
}
